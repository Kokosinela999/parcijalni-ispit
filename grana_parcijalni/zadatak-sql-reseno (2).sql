
CREATE TABLE Drzava(
    id INT AUTO_INCREMENT NOT NULL,
	code INT NOT NULL,
    naziv VARCHAR(50),
    PRIMARY KEY(id)
);

CREATE TABLE Film(
	id INT AUTO_INCREMENT NOT NULL,
    naziv VARCHAR(50) NOT NULL,
    godina INT NOT NULL,
    zanr VARCHAR(50) NOT NULL,
    drzava_porekla INT NOT NULL,
    PRIMARY KEY(id),
    FOREIGN KEY(drzava_porekla) REFERENCES Drzava(id)
);

CREATE TABLE Glumac(
	id INT AUTO_INCREMENT NOT NULL,
    ime VARCHAR(50) NOT NULL,
    prezime VARCHAR(50) NOT NULL,
    godina_rodjenja INT NOT NULL,
    pol VARCHAR(50) NOT NULL,
    drzavljanin INT NOT NULL,
    PRIMARY KEY(id),
    FOREIGN KEY(drzavljanin) REFERENCES Drzava(id)
);

CREATE TABLE Film_Glumac(
	film_id INT NOT NULL,
    glumac_id INT NOT NULL,
    FOREIGN KEY(film_id) REFERENCES Film(id),
    FOREIGN KEY(glumac_id) REFERENCES Glumac(id)
);

INSERT INTO Drzava(code, naziv) VALUES (10, 'Srbija');
INSERT INTO Drzava(code, naziv) VALUES (11, 'Severna Makedonija');
INSERT INTO Drzava(code, naziv) VALUES (12, 'Hrvatska');

SELECT * FROM Drzava;
DELETE FROM Drzava WHERE id = 1;

INSERT INTO Film(naziv, godina, zanr, drzava_porekla) VALUE ('Decko koji obecava', 1981, 'Drama', 2);
INSERT INTO Film(naziv, godina, zanr, drzava_porekla) VALUE ('Ko to tamo peva', 1978, 'Komedija', 2);
INSERT INTO Film(naziv, godina, zanr, drzava_porekla) VALUE ('Inception', 2018, 'Sci-fi', 3);
INSERT INTO Film(naziv, godina, zanr, drzava_porekla) VALUE ('The Departed', 2012, 'Krimi', 4);

SELECT * FROM Film;

INSERT INTO Glumac(ime, prezime, godina_rodjenja, pol, drzavljanin) VALUE('Leonardo', 'Di Caprio', 1974, 'muski', 3);
INSERT INTO Glumac(ime, prezime, godina_rodjenja, pol, drzavljanin) VALUE('Matt', 'Damon', 1976, 'muski', 4);
INSERT INTO Glumac(ime, prezime, godina_rodjenja, pol, drzavljanin) VALUE('Danilo', 'Stojkovic', 1938, 'muski', 2);
INSERT INTO Glumac(ime, prezime, godina_rodjenja, pol, drzavljanin) VALUE('Neda', 'Arneric', 1940, 'zenski', 2);
INSERT INTO Glumac(ime, prezime, godina_rodjenja, pol, drzavljanin) VALUE('Milos', 'Bikovic', 1995, 'zenski', 2);

SELECT * FROM Glumac;

INSERT INTO Film_Glumac(film_id, glumac_id) VALUE (2, 3);
INSERT INTO Film_Glumac(film_id, glumac_id) VALUE (2, 4);
INSERT INTO Film_Glumac(film_id, glumac_id) VALUE (3, 1);
INSERT INTO Film_Glumac(film_id, glumac_id) VALUE (4, 1);
INSERT INTO Film_Glumac(film_id, glumac_id) VALUE (4, 2);

SELECT * FROM Film_Glumac;

# Zadatak1

SELECT * FROM Film WHERE zanr = 'Komedija';

# Zadatak2

SELECT * FROM Glumac WHERE godina_rodjenja < 1992;

# Zadatak3
UPDATE Drzava SET naziv = 'Amerika' WHERE id = 4;
SELECT * FROM Glumac JOIN Drzava ON Drzava.id = Glumac.drzavljanin WHERE pol = 'zenski';

# Zadatak4
UPDATE Film SET godina = 2020 WHERE id = 2;
SELECT * FROM Film JOIN Drzava ON Film.drzava_porekla = Drzava.iD WHERE godina >= 2019 AND Drzava.naziv = 'Srbija';

# Zadatak5
SELECT COUNT(drzava_porekla), Drzava.Naziv FROM Film JOIN Drzava ON Drzava.Id = Film.drzava_porekla GROUP BY(Drzava.naziv); 

# Zadatak6
SELECT DISTINCT Film.naziv FROM Film
JOIN Drzava
ON Drzava.Id = Film.drzava_porekla
JOIN Film_Glumac
ON Film.Id = film_id
JOIN Glumac 
ON glumac_id = Glumac.id
WHERE drzava_porekla = drzavljanin;


